# mc:               # ID declaration
#   pkg:                # state declaration
#     - installed       # function declaration
mc:
  pkg.installed: []
    # - mc
    # - mcedit

/etc/vimrc:
  file.managed:
    - source: salt://files/vimrc
    - mode: 644
    - user: root
    - group: root
