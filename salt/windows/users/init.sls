# Set variables for current server
{% set server = salt.pillar.get('servers', {})[grains['id']] %}
{% set server_current_users = salt['user.list_users']() %}

###* User absense management *###
{% for managed_user in pillar['users'] %}
  {% if managed_user in server_current_users %} # Check if managed user is present in server configuration
    {% if ( not managed_user in server.users ) or ( salt['pillar.get']('users:'+managed_user+':fired') == True ) %} # If managed user do not found in server users of fired
user_absent_on_server {{ managed_user }}:
  user.absent:
    - name: {{ managed_user }}
    - force: True
    - purge: False

    {% endif %}
  {% endif %}
{% endfor %}
###* *###



###* User presence management *###

{% if ('users' in server) or ('admins' in server) %} 

  {% for user_name in ((server.users + server.admins) | unique) %}
  {% if ( (not 'fired' in pillar['users'][user_name]) or (salt['pillar.get']('users:'+user_name+':fired') != True) )%} # If user has no `fired` field or if `fired` value is not `True`

user_present_on_server {{ user_name }}:
  user.present:
    - name: {{ user_name }}

    {% if 'fullname' in pillar['users'][user_name] %} #TODO: Prettify?
    - fullname: {{ salt['pillar.get']('users:'+user_name+':fullname') }}
    {% endif %}

    {% if 'password' in pillar['users'][user_name] %}
    - password: {{ salt['pillar.get']('users:'+user_name+':password') }}
    {% endif %}

    - groups:
      # Add to Users group by default
          {% if  grains['locale_info'].defaultlanguage.startswith('ru') %}
        - Пользователи
          {% else %}
        - Users
          {% endif %}
      # Add to predefined Win groups - Admin
        {% if ( (pillar['users'][user_name]['global_admin'] == True) or (user_name in server.admins) ) %}
          {% if  grains['locale_info'].defaultlanguage.startswith('ru') %}
        - Администраторы
          {% else %}
        - Administrators
          {% endif %}
        {% endif %}
      # Add to predefined Win groups - Remote Desktop Users
        {% if pillar['users'][user_name]['global_rdp_user'] == True %}
          {% if  grains['locale_info'].defaultlanguage.startswith('ru') %}
        - Пользователи удаленного рабочего стола
          {% else %}
        - Remote Desktop Users
          {% endif %}
        {% endif %}


    # Iterate over optional_groups if exists
    {% if ('global_optional_groups' in pillar['users'][user_name]) %}
    - optional_groups:
        {% for group_name in pillar['users'][user_name]['global_optional_groups'] %}
      - {{ group_name }}
        {% endfor %}
    {% endif %}

  {% endif %}
  {% endfor %}

{% endif %}
###* *###
