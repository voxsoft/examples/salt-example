servers:

  VoxPC-WIN:
    users: [deleteme-user,  deleteme-admin]
    admins: [deleteme-fired]
    roles: [nginx, sftp, zabbix-agent]

  VoxPC-WIN-2:
    users: [deleteme-user]
    admins: []
    roles: [iis, zabbix-agent]
