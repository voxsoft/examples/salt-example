### Managed users ###
users:

  deleteme-user:
    fullname: "DeleteMe Just A User"
    global_admin: False
    global_rdp_user: True
    password: "P@ssw0rd1"
    global_optional_groups: [] # If a group specified here does not exist on the minion, the state will silently ignore it.
    fired: False

  deleteme-fired:
    fullname: "Deleteme Fired"
    global_admin: False
    global_rdp_user: False
    password: "P@ssw0rd1"
    global_optional_groups: [] # If a group specified here does not exist on the minion, the state will silently ignore it.
    fired: True

  deleteme-admin:
    fullname: "Deleteme Admin"
    global_admin: False
    global_rdp_user: False
    password: "P@ssw0rd1"
    global_optional_groups: [] # If a group specified here does not exist on the minion, the state will silently ignore it.
    fired: False
