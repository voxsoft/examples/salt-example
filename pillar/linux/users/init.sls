### Managed users ###

users:

  deleteme-user:
    fullname: "DeleteMe Just A User"
    global_admin: False
    ssh_pub_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSCirkYlIHQs0xYkdlEVS5b1jBsHJfvM0E2NMzOwamS5ET9XQYDUl+Tqg0Re14bLEcgQ3oD9qlnBVqowdGz2BMe+o5Wl1IauHt0BDfQ+q1UfMEGWejNpXHGlu+vGpMMq5ePwnYYEC/Gn2pN6XNQbs3I2L0y6W/uVXubEOK8h2DYPAgm3pi81Zpk3xiGnSNXnz0igMJtMEDy6iADOMgTBY2FUF9ylwuBCitjwcBJmPR3Puxw9i3TpVTrRG1J+XvBLunSaTSHyBvE8x8P+AFaTzw3nnz1vneK5B4Qj5SzDjGOTVbxcAcjqwjKiDooCbWqbytPcnv9phrC1bX0vsFP+W+CONIVadvBOVr7Ke7wtla/w8YvwDhQv4OmLTZFf915zH+CIrV92pJC4G4XMdQSHMF31bRF465PsisBI1Nv9v+LpYZHaW69I0MTigdplPrrrO9J4xruXw7/jSgRpYdjSnpXjWX9fhOoDQ9bAygDbJQxFmu79AEI3gvtqLJfP55Nkk= dummy@example.com
    global_optional_groups: [docker] # If a group specified here does not exist on the minion, the state will silently ignore it.
    fired: False

  deleteme-fired:
    fullname: "Deleteme Fired"
    global_admin: False
    ssh_pub_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSCirkYlIHQs0xYkdlEVS5b1jBsHJfvM0E2NMzOwamS5ET9XQYDUl+Tqg0Re14bLEcgQ3oD9qlnBVqowdGz2BMe+o5Wl1IauHt0BDfQ+q1UfMEGWejNpXHGlu+vGpMMq5ePwnYYEC/Gn2pN6XNQbs3I2L0y6W/uVXubEOK8h2DYPAgm3pi81Zpk3xiGnSNXnz0igMJtMEDy6iADOMgTBY2FUF9ylwuBCitjwcBJmPR3Puxw9i3TpVTrRG1J+XvBLunSaTSHyBvE8x8P+AFaTzw3nnz1vneK5B4Qj5SzDjGOTVbxcAcjqwjKiDooCbWqbytPcnv9phrC1bX0vsFP+W+CONIVadvBOVr7Ke7wtla/w8YvwDhQv4OmLTZFf915zH+CIrV92pJC4G4XMdQSHMF31bRF465PsisBI1Nv9v+LpYZHaW69I0MTigdplPrrrO9J4xruXw7/jSgRpYdjSnpXjWX9fhOoDQ9bAygDbJQxFmu79AEI3gvtqLJfP55Nkk= dummy@example.com
    global_optional_groups: [docker] # If a group specified here does not exist on the minion, the state will silently ignore it.
    fired: True

  deleteme-admin:
    fullname: "Deleteme Admin"
    global_admin: True
    ssh_pub_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSCirkYlIHQs0xYkdlEVS5b1jBsHJfvM0E2NMzOwamS5ET9XQYDUl+Tqg0Re14bLEcgQ3oD9qlnBVqowdGz2BMe+o5Wl1IauHt0BDfQ+q1UfMEGWejNpXHGlu+vGpMMq5ePwnYYEC/Gn2pN6XNQbs3I2L0y6W/uVXubEOK8h2DYPAgm3pi81Zpk3xiGnSNXnz0igMJtMEDy6iADOMgTBY2FUF9ylwuBCitjwcBJmPR3Puxw9i3TpVTrRG1J+XvBLunSaTSHyBvE8x8P+AFaTzw3nnz1vneK5B4Qj5SzDjGOTVbxcAcjqwjKiDooCbWqbytPcnv9phrC1bX0vsFP+W+CONIVadvBOVr7Ke7wtla/w8YvwDhQv4OmLTZFf915zH+CIrV92pJC4G4XMdQSHMF31bRF465PsisBI1Nv9v+LpYZHaW69I0MTigdplPrrrO9J4xruXw7/jSgRpYdjSnpXjWX9fhOoDQ9bAygDbJQxFmu79AEI3gvtqLJfP55Nkk= dummy@example.com
    global_optional_groups: [docker] # If a group specified here does not exist on the minion, the state will silently ignore it.
    fired: False
