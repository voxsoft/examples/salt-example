servers:

  VoxPC-wsl:
    users: [deleteme-user, deleteme-admin]
    admins: [deleteme-fired]
    roles: [apache, cert-bot, zabbix-agent]

  VoxPC-wsl-2:
    users: [deleteme-user]
    admins: []
    roles: [sftp, zabbix-agent]
